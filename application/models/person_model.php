<?php defined('BASEPATH') or exit('No direct script access allowed');

class Person_model extends CI_Model
{

    private $_table ='people';

    public $id;
    public $username;
    public $password;
    public $nama_lengkap;
    public $poto;

    public function rules()
    {
        return [
            [
                'field' => 'username',
                'label' => 'Username',
                'rules' => 'required'
            ],

            [
                'field' => 'password',
                'label' => 'Password',
                'rules' => 'required'
            ],

            [
               'field' => 'nama_lengkap',
               'label' => 'Nama Lengkap',
               'rules' => 'required'
            ],
        ];
    }

    public function getAll()
    {
        return $this->db->get($this->_table)->result();
    }
    public function getById($id)
    {
        return $this->db->get_where($this->_table,['id' => $id])->row();
    }

    public function insert($data)
    {
        return $this->db->insert($this->_table, $data);
    }

    public function update($data, $id)
    {
        $this->db->where('id', $id);
        $result = $this->db->update($this->_table, $data);
        return $result;
    }

    public function delete($id)
    {
        $this->_deleteImage($id);
        return $this->db->delete($this->_table, ['id' => $id]);
    }


    public function uploadImage($name)
    {
        $config['upload_path'] = './img/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['file_name'] = $name;
        $config['overwrite'] = true;
        $config['max_size'] = 1024; // 1MB
        // $config['max_width']            = 1024;
        // $config['max_height']           = 768;

        $this->load->library('upload', $config);

        if ($this->upload->do_upload('poto')) {
            return $this->upload->data("file_name");
        }
        // print_r($this->upload->display_errors());
        return "error";
    }

    private function _deleteImage($id)
    {
        $person = $this->getById($id);
        if ($person->poto != "") {
            $filename = explode(".", $person->poto)[0];
            return array_map('unlink', glob(FCPATH . ".img/$filename.*"));
        }
    }
}