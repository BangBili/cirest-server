<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Person extends RestController {
     function __construct()
     {
         // Construct the parent class
        parent::__construct();
        $this->load->database();
     }

     public function persons_get()
     {
          $id = $this->get('id');
          // $model =  $this->pegawai_model->getAll();
          if($id == "") {
               $person = $this->db->get('people')->result();
          } else {
               $this->db->where('id', $id);
               $people = $this->db->get('people')->result();
          }
          $this->response($person, 200);
     }
}

?>