<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;


class Api extends RestController
{
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->load->model('person_model', '', TRUE);
        $this->load->library('form_validation');
    }


    public function persons_get()
    {
        $id = $this->get('id');
       
        // $model =  $this->pegawai_model->getAll();
        if ($id == "") {
            $person = $this->person_model->getAll();
        } else {
            $this->db->where('id', $id);
            $person = $this->db->get('people')->result();
        }
        $this->response($person, 200);
    }

    public function persons_post()
    {
        $validation = $this->form_validation;
        $validation->set_rules('username', 'Name', 'required|is_unique[people.username]|trim');
        $validation->set_rules('password', 'Password', 'required|trim');
        $validation->set_rules('nama_lengkap', 'Password', 'required|trim');

        
        if (empty($_FILES["poto"]["name"])) {
            $this->response(array('status' => 'fail', 502));
        } else {

            $data = array(
                'username'     => $this->post('username'),
                
                'nama_lengkap'  => $this->post('nama_lengkap'),
                'poto'         => $this->person_model->uploadImage($_FILES["poto"]["name"]),
                'created_at'   => date("Y-m-d"),
                'password' => password_hash(
                    $this->post('password'),
                    PASSWORD_DEFAULT
                ),
            );

            if ($validation->run($data) == false) {
                $this->response(array('status' => 'Data masih ada yang kosong', 502));
            } else {
                $insert = $this->person_model->insert($data);
            }
            if ($insert) {
                $this->response($data, 200);
            } else {
                $this->response(array('status' => 'fail', 502));
            }
        }   
    }

    public function persons_put()
    {
        $id = $this->put('id');
        $data = array(
            'username'     => $this->put('username'),
          
            'nama_lengkap'  => $this->put('nama_lengkap'),
            'poto'         => $this->put('poto'),
            
        );
        
        $update = $this->person_model->update($data, $id);
        
        if ($update) {
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function persons_delete()
    {
        $id = $this->delete('id');
        $delete = $this->person_model->delete($id);
        

        if ($delete) {
            $data = [
                'message' => 'data sukses dihapus'
            ];
            $this->response($data, 200);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
    }

    public function login_post()
    {
        $username = $this->post('username');
        $password = $this->post('password');

        $user = $this->db->get_where('people', ['username' => $username])->row_array();
        
    
        if($user) {
            if(password_verify($password, $user['password'])) {
                $this->response(array('status' => 'Berhasil Login', 200));
            }
        } else {
            $this->response(array('status' => 'username tidak terdaftar', 401));
        }
    }
}
